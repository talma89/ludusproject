from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver

# Create your views here.

# Takes in a web request and returns a web response
def home(request):
    context = {}
    template = 'index.html'
    return render(request, template, context)
    
@login_required
def user_profile(request):
    if request.user.is_authenticated():
        user = request.user
    else:
        user = none
 
    context = {
             'user': user
  
        }
    if request.user.is_authenticated():
        User = get_user_model()
        queryset = User.objects.all()
    else:
        User = none
    context = {
              'queryset': queryset,
        }
    template = 'profile.html'
    
    return render(request, template, context)  
    
    #In the funtion when the user successfully goes to the account 
    #If request.user.is_authenticated():
        #context = {'title': 'Welcome', 'user': request.user}
    #else:
        #context = {'title': 'hello new user'}
    #Then go to the HTML file add a header lets say 
    #calls the context from view.py
    # <h1>{{ title }} {{user.email or user.name }} </h1>
