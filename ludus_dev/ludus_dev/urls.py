"""ludus_dev URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static 
from allauth.compat import importlib
from django.conf import settings
from contacts import views
#from signup import views



urlpatterns = [
    url(r'^$',views.home, name= 'home'), 
    url('^accounts/', include('allauth.urls')),
    url(r'^profile/', views.user_profile, name='profile'),
    # url(r'^home/', include('signup.urls')),
    # url(r'^login/', include('signup.urls')),
    url(r'^admin/', admin.site.urls),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# url('^accounts/', include('allauth.urls')),