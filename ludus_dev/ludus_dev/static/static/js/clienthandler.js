//Open socket between client and server, change local host to location of running server
var socket = io.connect('http://localhost:8080/');   

var session;
//get username from inject element from database, make SIP user agent
var name = document.getElementById('uname').textContent;
var firstname = document.getElementById('firstname').textContent;
var lastname = document.getElementById('lastname').textContent;
var userAgent = new SIP.UA(name+'@ludus.onsip.com');
var mycourses = [];
var inclass = {
    code: '',
    profname: '',
    profid: ''
};
var conmode = false;

//Upon connection, tell service my user name for client handling
socket.on('request name', function() {
    var names = {
        fname : firstname,
        lname : lastname,
        uid : name
    }
    socket.emit('send name', names);
});

//Server checking if user still online when disconnection detected
socket.on('connection check', function(myuname) {
    if (myuname == name) {
        console.log("I'm still here!");
        socket.emit('still here', name);
    }
});

//Function to display students currently online as active
socket.on('update active', function(users) {
    var friends = $("#friendlist li");
    friends.each(function(li) {
        var student = $(this).children().eq(0).text();
        var x;
        for (x in users) {
            if (users[x] == student) {
                $(this).addClass("active");
                $(this).trigger("to active");
                $("ul.friendlist").find(this).prependTo("#ul.friendlist");
                $(this).css("color","#33CC33");
            }
        }
    });
});

//Ensure busy students appear as orange in friend list
socket.on('update busy', function(busy) {
    var friends = $("#friendlist li");
    friends.each(function(li) {
        var student = $(this).children().eq(0).text();
        var x;
        for (x in busy) {
            if (busy[x] == student) {
                $(this).addClass("busy");
                $(this).removeClass("active");
                $("ul.friendlist").find(this).prependTo("#ul.friendlist");
                $(this).css("color","#fa4a1e");
            }
        }
    });
});

//Display active class as green
socket.on('active class', function(classname) {
    console.log(classname + " is now active.");
    var classes = $("ul.courses li");
    classes.each(function(li) {
        var aclass = $(this).text();
        console.log('Class from List: ' + aclass);
        if (classname == aclass) {
            $(this).addClass("active");
            $(this).css("color","#33CC33");
        }
    });
});

//Change a students name display to inactive at disconnect
socket.on('remove active', function(myuname) {
    var friends = $("#friendlist li");
    friends.each(function(li) {
        var student = $(this).children().eq(0).text();
        if (myuname == student) {
            $(this).removeClass("active");
            $(this).css('color','#999999');
        }
    });
});

//Change status from busy back to offline, online status handling occurs on reconnect
socket.on('remove busy', function(myuname) {
    var friends = $("#friendlist li");
    friends.each(function(li) {
        var student = $(this).children().eq(0).text();
        if (myuname == student) {
            console.log('Removing ' + student + ' from busy state.');
            console.log('Contents of this: ' + $(this).text());
            $(this).removeClass("busy");
            $(this).css('color','#999999');
        }
    });
});

//When a student leaves the class, remove his name from the list
socket.on('remove classmate', function(fullname) {
    console.log(fullname + ' has left class.');
    var classmates = $('ul.classmates li');
    classmates.each(function(li) {
        console.log($(this).text());
        if ($(this).text() == fullname) {
            console.log('Removing ' + fullname + " from classmates.");
            $(this).remove();
        }
    });
});

//Instructor leaves the class, deactivate the classroom and boot all students in session
socket.on('deactivate class', function(classname) {
    var classes = $("ul.courses li");
    classes.each(function(li) {
        var dclass = $(this).text();
        if (dclass == classname) {
            $(this).removeClass("active");
            $(this).css('color','#999999');
        }
    });
    if (inclass.code == classname) {
        alert('The instructor has left the class.');
        window.location.href = "/profile/";
        socket.emit('set to active', name);
    }
});

//Function to change view of webpage for 1 to 1 conferencing
function show1to1() {

    $('.welmsg').fadeOut('slow' ,function() {
        conmode = true;
        //$('#remoteVideo').css("display","block");
        $('#localVideo').addClass('localVid');
        $('#Video1').addClass('remoteVid');
        $('.localVid').show();
        $('.remoteVid').show();
        $('#Video2').hide();
        $('#Video3').hide();
        $('#Video4').hide();
        $('#Video5').hide();
        $('.videoContainter').show();
        $(".startclass").hide();
        $("#endbtn").show();
        $("#endbtn").css('visibility',"visible");
        $("#chngbtn").hide();
        $('.conmodemsg').hide();
        $("#welcome").animate({
            marginTop: "2vh",
            height: "83vh" 
        });
    });
}

//Settings for making 1 to 1 call
var options = {
    media: {
        constraints: {
            audio: true,
            video: true
        },
        render: {
            remote: document.getElementById('remoteVideo'),
            local: document.getElementById('localVideo')
        }
    }
};

//Dyanmic hover event for classes, can be made simpler if courses are persistent
$(document).on('mouseenter', 'li.course', function() {
    $(this).css('color','#FFFFFF');
});
$(document).on('mouseleave', 'li.course', function() {
    if ($(this).hasClass("active")) {
        $(this).css('color','#33CC33');
    } else {
        $(this).css('color','#999999');
    }
});

//Hover event for when mousing over friends
$('.friend').hover(
    function(){
        $(this).css('color','#FFFFFF');
    },
    function(){
        if ($(this).hasClass("active")) {
            $(this).css('color','#33CC33');
        } else if ($(this).hasClass("busy")){
            $(this).css('color','#fa4a1e');
        } else {
            $(this).css('color','#999999');
        }
    }
);

//////////////////////////////////////////////////////////////

var session0;
var session1;
var session2;
var session3;
var session4;
var session5;

var sessions = [session0, session1, session2, session3, session4, session5];
var usersConnected = [];    // stores all the current users
var numConnected = 0;   // indexing through all the users

//var v1 = document.getElementById('Video1');
var v2 = document.getElementById('Video2');
var v3 = document.getElementById('Video3');
var v4 = document.getElementById('Video4');
var v5 = document.getElementById('Video5');

var videoElement = [v2, v3, v4, v5];
var vNum = 0;
var firstConnection = true;

// media configuration for connecting to user 2
var option = {
    media: {
        constraints: {
            audio: true,
            video: true
        },
        render: {
            local: document.getElementById('localVideo'),
            remote: document.getElementById('Video1')
            
        }
    }
};

function op(){
    var o = {
        media: {
            constraints: {
                audio: true,
                video: true
            },
            render: {
                remote: videoElement[vNum]
            }
        }
    };
    return o
}

//Click a friends name to invite them to a 1 to 1 conversation
$('.friend').click(function(){
    if (conmode) {
        if ($(this).hasClass('active')) {
            socket.emit('busy', name);
            var options = op();
            var iname = $(this).children().eq(0).text();
            if(numConnected == 0){
                show1to1();
                //session1 = userAgent.invite(iname + '@ludus.onsip.com', option);
                userAgent.message(iname + '@ludus.onsip.com', name);
            }
            else{
                userAgent.message(iname + '@ludus.onsip.com', name);
                //session1 = userAgent.invite(iname + '@ludus.onsip.com', options);
                //vNum = vNum + 1;
                showMultiVid();
                for(i = 0; i < numConnected; i++){
                    userAgent.message(usersConnected[i] + '@ludus.onsip.com', iname);
                }
            }
            //firstConnection = false;    // after first connection is set
            usersConnected[numConnected] = iname;
            numConnected = numConnected + 1;
            console.log('vNum is at' + vNum);
        } else if ($(this).hasClass('busy')) {
            alert("This student is currently busy.");
        }
        else {
            alert("This student is not currently online.");
        }
    }
});

//Changing display from 1 to 1 to a multi feed view
function showMultiVid() {
    conmode = true;
    $('.welmsg').fadeOut('slow' ,function() {
        //$('#remoteVideo').css("display","block");
        $('#localVideo').hide();
        $('#localVideo').removeClass('localVid');
        $('#localVideo').show();
        $('#Video1').hide();
        $('#Video1').removeClass('remoteVid');
        $('#Video1').show();
        $('#Video2').show();
        $('#Video3').show();
        $('.videoContainter').show();
        $(".startclass").hide();
        $("#endbtn").show();
        $("#endbtn").css('visibility',"visible");
        $("#chngbtn").hide();
        $('.conmodemsn').hide();
        $("#welcome").animate({
            marginTop: "2vh",
            height: "570px" 
        });
    });
}

// accepting the SIP connection from host user and
// media configuration for connecting to host user
userAgent.on('invite', function(session) {
    console.log('Calling prof as userID: ' + inclass.profid);
    if (inclass.profid == name) {
        console.log('Successfully called professor.');
        $('#profvid').show();
        var profVid = document.getElementById('profvid');
        $('#profvid').prop('muted',true);
        session.accept({
            media: {
                constraints: {
                    audio: true,
                    video: true
                },
                render: {
                    local: profVid,
                    //remote: studentVid
                }
            }
        });
    } else {
        
        if(firstConnection == true){
            socket.emit('busy', name);
            show1to1();
            session.accept({
                media: {
                    constraints: {
                        audio: true,
                        video: true
                    },
                    render: {
                        local: document.getElementById('localVideo'),
                        remote: document.getElementById('Video1')
                    }
                }
            });
        }
        
        else{
            socket.emit('busy', name);
            session.accept({
                media: {
                    constraints: {
                        audio: true,
                        video: true
                    },
                    render: {
                        remote: videoElement[vNum]
                    }
                }
            });
            vNum = vNum + 1;
            showMultiVid();
        }
        firstConnection = false;
    }
});

// connect the other people together
userAgent.on('message', function(msg){
    var options = op();
    user = msg.body + '@ludus.onsip.com';
    console.log(msg.body);
    console.log(user);
    usersConnected[numConnected] = msg.body;
    numConnected = numConnected + 1;
    if(firstConnection == true){
        socket.emit('busy', name);
        show1to1();
        session0 = userAgent.invite(user, option);
        firstConnection = false;
    }
    else{
        socket.emit('busy', name);
        sessions[vNum] = userAgent.invite(user, options);
        vNum = vNum + 1;
        showMultiVid();
    }
    
});

//Show text messaging box
$('.sndmsg').click(function(){
    $('#chatbox').show(); 
    $('#chatbox').draggable({
        containment: "window"
    });
});

//Sends text message to server for distribution, id will be given
$('#chatform').submit(function(){
    var message = {
        snd: name,
        txt: $('#msg').val()
    };
    socket.emit('chat message', message);
    $('#msg').val('');
    return false;
});

//Send text message from within a classroom to the server for processing
$('#classchatform').submit(function(){
    var message = {
        snd: name,
        classcode: $('#coursecode').text(),
        txt: $('#classmsg').val()
    };
    socket.emit('classchat message', message);
    $('#classmsg').val('');
    return false;
});

//Close chat box
$('#xmsg').click(function(){
    $('#chatbox').hide(); 
});

//Show prompt to create a new course 
$('.createbtn').click(function(){
    $('#createcourse').show();
});

//Hide create course prompt
$('#xcreate').click(function() {
    $('#createcourse').hide();
});

//Change center view to set up multi-peer conference
$('.strtcon').click(function(){
     $('.welmsg').hide();
     $('.strtcon').hide();
     $('#conform').fadeIn('slow');
});

//Send new course and professor details to server for handling
$('.ncbtn').click(function() {
    var coursedets = {
        code : $('#newcourse').val(), 
        prof : name 
    }
    socket.emit('new course', coursedets);
    $('#newcourse').val('');
});

//Prof starts a new class and view changes to that class
$("ul.classes").on("click", "li.coursedd", function(){
    socket.emit('busy', name);
    var classname = $(this).text();
    $('#profname').text('Instructor: ' + firstname + ' ' + lastname);
    inclass.code = classname;
    inclass.profid = name;
    console.log("Entering Class: " + classname);
    $('#coursecode').text(classname);
    $('#sublock').fadeOut('slow', function() {
        $('#classblock').fadeIn('slow');
    });
    socket.emit('starting class', classname);
});

//Receive and display text message from server    
socket.on('receive msg', function(msg) {
    console.log("Recieved Message: " + msg);
    $('#chatbox').show();
    $('#chatbox').draggable({
        containment: "window"
    });
    $('#messages').append($('<li>').text(msg));
    $('#messages').scrollTop($('#messages').height());
});

//Display in class text received from the server
socket.on('receive class msg', function(msg) {
    console.log("Received Message: " + msg.text);
    if (msg.fromprof == true) {
        $('#classmessages').append($('<li>').text(msg.text).css('color','#fa4a1e'));
    } else {
        $('#classmessages').append($('<li>').text(msg.text));
    }
    $('#classmessages').scrollTop($('#messages').height());
});

//Display courses in server data array, checks if already present
socket.on('courses', function(courses) {
    var x;
    for (x in courses) {
        var havecourse = false;
        var courselist = $('ul.courses li');
        courselist.each(function(li) {
            if ($(this).text() == courses[x]) {
                havecourse = true;
            }
        });
        if(!havecourse) {
            $('ul.courses').append($('<li>').addClass('course').text(courses[x]));
            //$('ul.classes').append($('<li>').addClass('coursedd').text(courses[x]));
        }
    }
});

//Add classes that you teach to start class drop down
socket.on('your class', function(course) {
    if (mycourses.indexOf(course) == -1) {
        mycourses.push(course);
        $('ul.classes').append($('<li>').addClass('coursedd').text(course));
    }
});

//Notify that class already exists if you try and create it again
socket.on("add error", function() {
    alert("Course Already Exists.");
});

//Click event for a student attempting to join the classroom
$('ul.courses').on('click','li.course', function() {
    if ($(this).hasClass("active")) {
        socket.emit('busy', name);
        var classname = $(this).text();
        var joinclass = {
            class : classname,
            name: name
        }
        inclass.code = classname;
        socket.emit('entering class', joinclass);
        socket.emit('get profname', joinclass);
        $('#coursecode').text(classname);
        $('#sublock').fadeOut('slow', function() {
            $('#classblock').fadeIn('slow');
        });
    } else {
        alert("This class is not currently in session.");
    }
});

//Get the professors name and invite the professor to send his video feed
socket.on('send profname', function(response) {
    if (response.classname == inclass.code) {
        $('#profname').text('Instructor: ' + response.profname);
        inclass.profid = response.profid;
        var profVid = document.getElementById('profvid');
        var stuVid = document.getElementById('studentvid');
        var lectureop = {
            media: {
                constraints: {
                    audio: true,
                    video: true
                },
                render: {
                    remote: profVid
                    //local: stuVid
                }
            }
        }
        console.log('calling prof at userID: ' + inclass.profid);
        userAgent.invite(inclass.profid + '@ludus.onsip.com', lectureop);
        $('#profvid').show();
    }
});

//Receive list of classmates and display new list
socket.on('send classmates', function(studentnames) {
    var x;
    var text = '';
    for (x in studentnames) {
        var notlisted = true;
        var classmates = $('ul.classmates li');
        classmates.each(function(li) {
            if($(this).text() == studentnames[x]) {
                notlisted = false;
            }
        });
        if(notlisted) {
            $('ul.classmates').append($('<li>').addClass('active').text(studentnames[x]));
        }
        text = text + studentnames[x] + ', ';
    }
    console.log('Currently in class: ' + text);
});

//Clicking End Call or Leave Class Buttons that redirect to the home page
$("#endbtn").click(function(){
    backToHome();
});

$("#lvbtn").click(function(){
    backToHome();
});

$("#bkbtn").click(function(){
    backToHome();
});

function backToHome() {
    socket.emit('set to active', name);
    window.location.href = "/profile/";
}

//Button that changes the display to notify that conferencing mode is now enabled
$("#chngbtn").click(function(){
    conmode = true;
    $('.welmsg').fadeOut('slow',function(){
        $('.startclass').hide();
        $('.chngbtn').hide();
        $('.conmodemsg').fadeIn('slow');
    });
});