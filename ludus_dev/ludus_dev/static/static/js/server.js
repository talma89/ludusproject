var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var userids = [];
var users = [];
var busy = [];
var fullnames = [];
var courses = [];
var courseprofs = [];
var activeclasses = [];

//Hosting static files, probably not necissary anymore
app.use(express.static('frontend'));

//Socket is now open to a client, all activity takes place within
//this event while connection persists
io.on('connection', function(socket) {
    var myuname;
    var mycourses = [];

    //Get username from client who just connected
    io.to(socket.id).emit('request name');
    socket.on('send name', function(names) {
        //handle connection and notify all connected users
        console.log(names.fname + " " + names.lname + " has logged in.");
        fullnames[names.uid] = "" + names.fname + " " + names.lname;
        connect(names.uid);
        update();
    });
    
    //Receive text message from client and emit to all students
    socket.on('chat message', function(message) {
        console.log("Message received from " + myuname + ": " + message.txt);
        var msg = message.snd + ": " + message.txt;
        io.emit('receive msg', msg);
    });

    //Receive a text message from student in a class room and send it to all other students present
    socket.on('classchat message', function(message) {
        console.log("Message received from " + myuname + ": " + message.txt + " --for students in " + message.classcode);
        var x;
        var fromprof = false;
        if (message.snd == courseprofs[message.classcode]) {
            fromprof = true;
        }
        var msg = {
            text : message.snd + ": " + message.txt, 
            fromprof: fromprof
        }
        for (x in activeclasses) {
            if (message.classcode == activeclasses[x].code) {
                var classmates = activeclasses[x].students;
                var y;
                for (y in classmates) {
                    console.log('Sending message to: ' + userids[classmates[y]].sockid);
                    io.to(userids[classmates[y]].sockid).emit('receive class msg', msg);
                }
                io.to(userids[courseprofs[activeclasses[x].code]].sockid).emit('receive class msg', msg);
            }
        }
    });

    //confirm user is still connected and perform reconnect for data purposes
    socket.on('still here', function(name) {
        connect(name);
        update();
    });

    //Handle creating of new course and store the relevant information
    socket.on('new course', function(coursedets) {
        if (courses.indexOf(coursedets.code) == -1) {
            var course = coursedets.code;
            var prof = coursedets.prof;
            courseprofs[course] = prof;
            courses.push(course);
            console.log("New Course " + course + " was added.");
            io.emit('courses', courses);
            var x;
            for (x in courses) {
                if (courseprofs[courses[x]] == myuname ) {
                    //if user of this connection is the prof, tell them
                    io.to(socket.id).emit('your class', courses[x]);
                }
            }
        } else {
            //Error handling if course exists 
            console.log("Course already exists");
            io.to(socket.id).emit("add error");
        }
    });

    //Class is in session, display it as such for other users online
    socket.on('starting class', function(classname) {
        console.log(classname + ' is now starting.');
        activeclasses.push({
            code: classname,
            students: []
        });
        var x;
        for (x in activeclasses) {
            io.emit('active class', activeclasses[x].code);
        }
    });

    //Student has entered the classroom, all other students are then informed
    socket.on('entering class', function(joinclass) {
        var x;
        console.log(joinclass.name + ' is joining class ' + joinclass.class);

        for (x in activeclasses) {
            var y;
            if (activeclasses[x].code == joinclass.class) {
                if (activeclasses[x].students.indexOf(joinclass.name) == -1) {
                    activeclasses[x].students.push(joinclass.name);
                }
                var text = "";
                var students = activeclasses[x].students;
                for (y in students){
                    text = text + students[y] + " "; 
                    console.log('Socket ID: ' + userids[students[y]].sockid);
                }
                console.log("Students in " + joinclass.class + ": " + text);
                sendStudents(x);
            }
        }
    });

    //Send client the professors name for display on the interface
    socket.on('get profname', function(joinclass) {
        console.log('Getting instructor of ' + joinclass.class);
        var profname = fullnames[courseprofs[joinclass.class]];
        var response = {
            classname: joinclass.class,
            profid: courseprofs[joinclass.class],
            profname: profname
        };
        var reqsock = userids[joinclass.name].sockid
        io.to(reqsock).emit('send profname', response);
    });

    socket.on('busy', function(name) {
        busy.push(name);
        update();
    });

    //Function that is called when sending the list of students to everyone in the classroom
    function sendStudents(x) {
        var y;
        var studentlist = activeclasses[x].students;
        var studentnames = [];
        for (y in studentlist) {
            studentnames.push(fullnames[studentlist[y]]);
        }
        var z;
        for (z in studentlist) {
            console.log('Sending classmates to ' + studentlist[z]);
            io.to(userids[studentlist[z]].sockid).emit('send classmates', studentnames);
        }
        io.to(userids[courseprofs[activeclasses[x].code]].sockid).emit('send classmates', studentnames);
    };
    //Change a users status from busy to active
    socket.on('set to active', function(name) {
        var j = busy.indexOf(name);
        busy.splice(j, 1);
        console.log('Removing ' + name + ' from busy list.');
        io.emit('remove busy', name);
        update();
    });

    //When user disconnects, ensure they aren't online in another window
    socket.on('disconnect', function() {
        console.log(myuname + " has disconnected.");
        delete userids[myuname];
        var i = users.indexOf(myuname);
        users.splice(i, 1);
        var j = busy.indexOf(myuname);
        busy.splice(j, 1);
        io.emit("connection check", myuname);
        io.emit("remove active", myuname);
        io.emit("remove busy" , myuname);
        var x;
        for (x in activeclasses) {
            if (activeclasses[x].students.indexOf(myuname) != -1) {
                var i = activeclasses[x].students.indexOf(myuname);
                activeclasses[x].students.splice(i, 1);
                var students = activeclasses[x].students;
                var y;
                for (y in students) {
                    var sockid = userids[students[x]].sockid;
                    io.to(sockid).emit('remove classmate', fullnames[myuname]);
                }
                var profsock = userids[courseprofs[activeclasses[x].code]].sockid;
                io.to(profsock).emit('remove classmate', fullnames[myuname]);
            }
            if (myuname == courseprofs[activeclasses[x].code]) {
                var classname = activeclasses[x].code;
                activeclasses.splice(x, 1);
                io.emit('deactivate class', classname);
            }
        }
    });

    //Handle user connection and store relevent information
    function connect(name) {
        myuname = name;
        userids[myuname] = {
            sockid: socket.id,
            busy: false
        }
        if (users.indexOf(myuname) == -1)
        {
            users.push(myuname);
        }
    }

    //Update contact list of all connected users
    function update() {
        var text = '';
        var x;
        for (x in users) {
            text = text + users[x] + ' ';
        }
        console.log("Current active users: " + text);
        //Give user course list upon connection
        io.emit('update active', users);
        io.emit('update busy', busy);
        io.emit('courses', courses);
        var y;
        for (y in courses) {
            if (courseprofs[courses[y]] == myuname ) {
                io.to(socket.id).emit('your class', courses[y]);
            }
        }
        var z;
        for (z in activeclasses) {
            console.log(activeclasses[z].code + " is active");
            var classname = activeclasses[z].code;
            io.emit('active class', classname);
        }
    }
});

//Run server to listen for requests at port 8080
http.listen(process.env.PORT || 8080, function() {
    console.log('listening on port: 8080');
});