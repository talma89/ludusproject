Running the Application

Database:

From workplace: 

1, activate virtualenv: source ludus_env1/bin/activate


2, start  database
		sudo service postgresql start

3, cd ludus_dev

python manage.py migrate --not always necissary but good to make sure


4, run server : python manage.py runserver 0.0.0.0:8000


Signalling Server:

1, go to server location - cd ludus_dev/ludus_dev/static/static/js/

2, install packages (if not installed) - npm install

3, run server - node server.js

Notes: --Packages required to set up the Django Web Framework are listed in requirements.txt. 
--There were issues installing all the required packages on MacOS, so we assume a linux based system will be needed. 
--The application runs on firefox and on chrome, but chrome is preferred since firefox requires you to accept that your webcam and mic
is shared every time a user connects to you. Working around this is a work in progress. Ensure you have the latest version of either browser.
--OnSIP also only offers the use of 10 users for free, and these are already in use. Therefor, no new users added will be able to make video calls.
This could have been avoided by setting up our own SIP server using third-party frameworks, such as asterisk or freeswitch. Unfortunately, a server based operating system (CentOS) is required to run such a server. The onSIP server was then selected for demonstration purposes. 
--If you wish to access the server running the application from another computer within a network, the IP address must be changed in both 
clienthandler.js and profile.html where specified. This IP address must reflect the private IP address of the computer running the server.
Access to this private address is then required if you wish to connect to the application. These are all due to the local nature of our demonstration. Hosting the application on a cloud server for public use is currently a work in progress.  

